/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "grey-100": "#e5e5e5ff",
        "grey-200": "hsla(0,0%,89.8%,.7)",
        "grey-300": "#333333b3",
        "light-dark": "#141414",
        purple: "#9333ea",
        black: "#000",
        white: "#fff",
        danger: "#dc2626",
      },
      textColor: {
        primary: "#e5e5e5",
        secondary: "#000",
        danger: "#ff2e2ec0",
      },
      fontSize: {
        base: "1rem",
        lg: "1.1rem",
        xl: "1.2rem",
        "2xl": "1.625rem",
        "3xl": "2rem",
        "4xl": "2.5rem",
      },
      borderRadius: {
        md: "5px",
        lg: "12px",
      },
      backgroundImage: {
        search: "url('./assets/images/search.png')",
      },
    },
    fontFamily: {
      sans: ["Roboto", "sans-serif"],
    },
  },
  plugins: [],
};
