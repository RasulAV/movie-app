import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "./store";

const languageStateSelector = (state: RootState) => state.languageReducer;

export const languageSelector = createSelector(
  languageStateSelector,
  (state) => state.curLang
);
