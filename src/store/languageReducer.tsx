import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Language } from "components/types";

export type State = {
  curLang: Language;
};

type Reducers = {
  changeLang: (state: State, action: PayloadAction<Language>) => void;
};

const languageSlice = createSlice<State, Reducers>({
  name: "language switcher/",
  initialState: {
    curLang: "en-ES",
  } as State,
  reducers: {
    changeLang(state, action) {
      state.curLang = action.payload
    },
  },
});

const languageReducer = languageSlice.reducer;
export const { changeLang } = languageSlice.actions;
export default languageReducer;
