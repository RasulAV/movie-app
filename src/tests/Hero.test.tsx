import userEvent from "@testing-library/user-event";
import Hero from "components/Hero";
import { MovieListItemType } from "components/types";
import { render, screen } from "test-utils";

const heroData = {
  adult: false,
  backdrop_path: "/hpzQHv8cA7j2Dn2CphOFYmllXzj.jpg",
  genre_ids: [18],
  id: 207,
  original_language: "en",
  original_title: "Dead Poets Society",
  overview:
    "At an elite, old-fashioned boarding school in New England, a passionate English teacher inspires his students to rebel against convention and seize the potential of every day, courting the disdain of the stern headmaster.",
  popularity: 36.12,
  poster_path: "/ai40gM7SUaGA6fthvsd87o8IQq4.jpg",
  release_date: "1989-06-02",
  title: "Dead Poets Society",
  video: false,
  vote_average: 8.3,
  vote_count: 9367,
};

describe("Hero component", () => {
  test("render Hero comp with non-empty data", async() => {
    const description =
      "At an elite, old-fashioned boarding school in New England, a passionate English teacher inspires his students to rebel against convention and seize the";
    const parsedDescription = description.substring(0, 149) + "...";
    render(<Hero heroData={heroData} />)
    expect(await screen.findByText(/Details/)).toBeInTheDocument();
    expect(await screen.findByText(/Dead Poets Society/)).toBeInTheDocument();
    expect(await screen.findByText(parsedDescription)).toBeInTheDocument();
  });

  test("render Hero comp with empty data", async() => {
    const emptyData = {} as MovieListItemType;
    render(<Hero heroData={emptyData} />)
    expect(await screen.findByText(/Loading/)).toBeInTheDocument();
  });

  test("click to details button", async () => {
    render(<Hero heroData={heroData} />)
    const button = await screen.findByText(/Details/);
    userEvent.click(button);
    expect(screen.getByRole("link")).toHaveAttribute("href", "/movie/207");
  });
});
