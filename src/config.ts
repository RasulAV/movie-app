const env = process.env;
const config = {
  service: {
    BASE_URL: env.REACT_APP_BASE_URL,
    API_KEY: env.REACT_APP_API_KEY,
    BASE_IMAGE_URL: env.REACT_APP_IMAGE_URL,
    YOUTUBE_URL: env.REACT_APP_YOUTUBE_URL,
  },
  language: {
    default: "en",
    available: [
      {
        lang: "en",
      },
      {
        lang: "ru",
      },
    ],
  },
};

export default config;
