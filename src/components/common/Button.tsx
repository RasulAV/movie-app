import classNames from "classnames";

type ButtonProps = {
  title: string;
  size?: "sm" | "md";
  className?: string;
  onClick?: <T>(...args: T[]) => void;
};

const buttonVariants = {
  sm: "py-[0.7em] px-[1.4em]",
  md: "py-[0.8em] px-[1.4em] md:py-[0.85em] md:px-[1.75em] lg:text-lg",
};

const buttonDefaultStyle =
  "mb-[0.85em] font-bold rounded-md bg-grey-300 hover:bg-grey-200 hover:text-secondary transition duration-100";

const Button: React.FC<ButtonProps> = ({
  title,
  size = "sm",
  className = buttonDefaultStyle,
  onClick,
}) => {
  return (
    <button
      className={classNames(className, buttonVariants[size])}
      onClick={onClick}
    >
      {title}
    </button>
  );
};

export default Button;
