import { useTranslation } from "react-i18next";

type ErrorProps = {
  error: string;
};

const Error: React.FC<ErrorProps> = ({ error }) => {
  const { t } = useTranslation();

  return (
    <div className="flex justify-center items-center mb-6">
      <p className="text-center text-xl md:text-2xl font-bold">{`${t(
        "common.error"
      )} ${error}`}</p>
    </div>
  );
};

export default Error;
