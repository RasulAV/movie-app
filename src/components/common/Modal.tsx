import { ReactNode } from "react";

type ModalProps = {
  isActive: boolean;
  handleModalSwitch: (value: boolean) => void;
  children?: ReactNode;
};

const Modal: React.FC<ModalProps> = ({
  isActive,
  handleModalSwitch,
  children,
}) => {
  return (
    <div
      className={`${
        isActive
          ? "opacity-100 pointer-events-auto"
          : "opacity-0 pointer-events-none"
      } fixed top-0 left-0 w-full h-full flex justify-center items-center bg-black/80 z-50`}
      onClick={() => handleModalSwitch(false)}
    >
      <div
        className={`${
          isActive ? "translate-y-0" : "translate-y-full"
        } relative w-[80vw] transition-all`}
      >
        <span
          className="absolute top-0 -right-[35px] w-[30px] h-[30px] cursor-pointer group"
          onClick={() => handleModalSwitch(false)}
        >
          <span className="absolute content-none top-[10px] left-0 w-[30px] h-[2px] bg-grey-200 group-hover:bg-white rotate-45"></span>
          <span className="absolute content-none top-[10px] left-0 w-[30px] h-[2px] bg-grey-200 group-hover:bg-white -rotate-45"></span>
        </span>
        {children}
      </div>
    </div>
  );
};

export default Modal;
