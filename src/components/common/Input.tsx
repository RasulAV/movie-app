import { KeyboardEvent } from "react";

type InputProps = {
  searchValue: string;
  changeSearchValue: (arg0: string) => void;
  placeholder: string;
};

const Input: React.FC<InputProps> = ({
  searchValue,
  changeSearchValue,
  placeholder,
}) => {
  const stopDefaultAction = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.code === "Enter") {
      e.preventDefault();
    }
  };

  return (
    <form action="" className="lg:flex lg:justify-end lg:w-[18em]">
      <input
        className="w-[14em] md:w-[12em] lg:w-[14em] py-2 pl-2 pr-8 rounded-md border-[.1em] border-slate-300 bg-transparent bg-search bg-no-repeat bg-[center_right_0.5em] outline-none lg:blur:transition duration-300 lg:focus:w-[18em]"
        type="text"
        placeholder={placeholder}
        value={searchValue}
        onChange={(e) => changeSearchValue(e.target.value)}
        onKeyDown={(e) => stopDefaultAction(e)}
      />
    </form>
  );
};

export default Input;
