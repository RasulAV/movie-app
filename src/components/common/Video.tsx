import React from "react";

type VideoProps = {
  modalActive?: boolean;
  title: string;
  videoSrc: string;
};

const Video: React.FC<VideoProps> = ({ modalActive, title, videoSrc }) => {
  return (
    <>
      {modalActive && (
        <iframe
          title={title}
          className="aspect-video w-full bg-black"
          src={videoSrc + `&autoplay=${modalActive}`}
          allow={
            "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          }
          allowFullScreen
        />
      )}
    </>
  );
};

export default Video;
