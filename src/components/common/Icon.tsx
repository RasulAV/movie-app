import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import classNames from "classnames";

type IconProps = {
  icon: IconDefinition;
  size?: "sm" | "md";
};

const iconVariants = {
  sm: "w-6 h-6 opacity-70",
  md: "w-9 h-9 text-primary",
};

const Icon: React.FC<IconProps> = ({ icon, size = "sm" }) => {
  return (
    <FontAwesomeIcon
      icon={icon}
      className={classNames(
        "hover:text-danger transition duration-200",
        iconVariants[size]
      )}
      inverse
    />
  );
};

export default Icon;
