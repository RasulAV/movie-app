import { FC } from "react";
import classNames from "classnames";
import { faQuestion } from "@fortawesome/free-solid-svg-icons";
import Icon from "./Icon";
import config from "config";

type MovieCoverProps = {
  imageUrl: string | null;
  movieTitle: string;
  size?: "sm" | "md" | "lg";
  clickable?: boolean;
  withBorder?: boolean;
};

const posterWidth = {
  sm: "w185",
  md: "w185",
  lg: "w342",
};

const movieCoverVariants = {
  sm: "h-[13.75em] w-[9em] md:h-[14.3em] md:w-[9.5em]",
  md: "h-[14em] w-[9.7em] md:h-[16em] md:w-[10em]",
  lg: "h-[17em] w-[11.3em] md:h-[25em] md:w-[16em]",
};

const baseImageUrl = config.service.BASE_IMAGE_URL;

const MovieCover: FC<MovieCoverProps> = ({
  size = "sm",
  clickable = true,
  movieTitle,
  imageUrl,
  withBorder = false,
}) => {
  return (
    <div className={classNames("shrink-0", movieCoverVariants[size])}>
      {imageUrl !== null ? (
        <img
          className={classNames(
            "object-cover h-full w-full",
            clickable &&
              "cursor-pointer transform duration-500 hover:scale-105",
            withBorder && "rounded-md"
          )}
          src={`${baseImageUrl}/${posterWidth[size]}${imageUrl}`}
          alt={movieTitle}
        />
      ) : (
        <div
          className={classNames(
            "h-full flex flex-col justify-center items-center border-2 border-slate-800",
            clickable && "cursor-pointer transform duration-500 hover:scale-105"
          )}
        >
          <Icon icon={faQuestion} />
          <p className="text-center">{movieTitle}</p>
        </div>
      )}
    </div>
  );
};

export default MovieCover;
