export type ParticipantsType = {
  id: number;
  name: string;
  profile_path: null | string;
  character?: string;
  job?: string;
};

export type GenresType = { id: number; name: string };

type Videos = {
  id: string;
  key: string;
  name: string;
};

export type MovieInfoType = {
  backdrop_path: string;
  poster_path: string;
  original_title: string;
  credits: {
    cast: ParticipantsType[];
    crew: ParticipantsType[];
  };
  runtime: number;
  genres: GenresType[];
  overview: string;
  similar: {
    page: number;
    results: { id: number; poster_path: string; title: string }[];
  };
  videos: {
    results: Videos[];
  };
};

export type HomeTitles =
  | "trending"
  | "popular"
  | "comedy"
  | "action"
  | "romantic"
  | "crime"
  | "horror"
  | "new"
  | "documentaries";

export type MovieListItemType = {
  adult: boolean;
  backdrop_path: string;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  release_date: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
};

export type SearchingData = {
  original_title: string;
  id: number;
  poster_path: string | null;
};

export type Language = "en-ES" | "ru-RU";
