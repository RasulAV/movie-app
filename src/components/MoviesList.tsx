import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import "swiper/css";
import { Swiper, SwiperSlide } from "swiper/react";
import MovieCover from "./common/MovieCover";
import Spinner from "./common/Spinner";
import { HomeTitles, MovieListItemType } from "./types";

type MoviesListProps = {
  title: HomeTitles;
  moviesList: [MovieListItemType[], string?];
};

const MoviesList: React.FC<MoviesListProps> = ({ title, moviesList }) => {
  const { t } = useTranslation();
  const titles = {
    trending: t("home.rated"),
    popular: t("home.popular"),
    comedy: t("home.comedy"),
    action: t("home.action"),
    romantic: t("home.romantic"),
    crime: t("home.crime"),
    horror: t("home.horror"),
    documentaries: t("home.documentaries"),
    new: t("home.new"),
  };

  return (
    <div className="p-5">
      <h2
        id={moviesList[1] !== undefined ? `${moviesList[1]}` : undefined}
        className="mb-2 text-3xl font-bold"
      >
        {titles[title]}
      </h2>
      <div>
        <Swiper
          spaceBetween={26}
          slidesPerView="auto"
          className="h-[15em] md:h-[17em]"
        >
          {moviesList !== undefined ? (
            moviesList[0].map((movie) => {
              const { original_title, poster_path, id } = movie;
              return (
                <SwiperSlide
                  key={id}
                  className="flex justify-center items-center w-[11em]"
                >
                  <Link to={`/movie/${id}`}>
                    <MovieCover
                      size="md"
                      movieTitle={original_title}
                      imageUrl={poster_path}
                    />
                  </Link>
                </SwiperSlide>
              );
            })
          ) : (
            <Spinner />
          )}
        </Swiper>
      </div>
    </div>
  );
};

export default MoviesList;
