import React from "react";
import { faGithubAlt, faLinkedinIn } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";

import Icon from "./common/Icon";

const Footer: React.FC = () => {
  const iconsLinks = [
    { icon: faEnvelope, link: "#" },
    { icon: faGithubAlt, link: "#" },
    { icon: faLinkedinIn, link: "#" },
  ];
  return (
    <div className="bg-light-dark">
      <ul className="flex justify-center gap-7 py-6 md:py-12">
        {iconsLinks.map((item, index) => {
          return (
            <li key={index}>
              <a href={item.link}>
                <Icon icon={item.icon} />
              </a>
            </li>
          );
        })}
      </ul>
      <div className="flex justify-center pb-4 text-sm opacity-60 cursor-pointer hover:text-danger transition duration-200">
        &copy;2022 Copyright
      </div>
    </div>
  );
};

export default Footer;
