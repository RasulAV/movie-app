import { Link, useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import Modal from "components/common/Modal";
import MovieInfo from "components/MovieInfo";
import Participants from "components/Participants";
import MovieCover from "components/common/MovieCover";
import Video from "components/common/Video";
import axiosInstance from "api/axiosInstance";
import errorHandler from "api/errorHandler";
import { AxiosError } from "axios";
import { useTranslation } from "react-i18next";
import config from "config";
import Error from "./common/Error";
import Spinner from "./common/Spinner";
import { Swiper, SwiperSlide } from "swiper/react";
import { MovieInfoType } from "./types";
import { useSelector } from "react-redux";
import { languageSelector } from "store/selectors";

const youtubeUrl = config.service.YOUTUBE_URL;

const MovieDetails: React.FC = () => {
  const [isActiveModal, setIsActiveModal] = useState(false);
  const [error, setError] = useState<string>("");
  const [movieDetailsData, setMovieDetailsData] =
    useState<MovieInfoType | null>(null);
  const curLang = useSelector(languageSelector);
  const location = useLocation();
  const { t } = useTranslation();

  const handleModalSwitch = (value: boolean) => {
    setIsActiveModal(value);
  };

  useEffect(() => {
    const fetchData = async (id: number) => {
      const params = { append_to_response: "videos,credits,similar" };
      await axiosInstance(`/movie/${id}`, {
        params: { ...params, language: curLang },
      })
        .then((response) => {
          setMovieDetailsData(response.data);
        })
        .catch((err) => {
          const newError = errorHandler(err as AxiosError | Error);
          setError(newError);
        });
    };
    const params: number | null = parseInt(location.pathname.substring(7));
    if (!isNaN(params)) {
      fetchData(params);
    }
  }, [location.pathname, curLang]);

  return (
    <>
      {error.length !== 0 && (
        <div className="pt-64">
          <Error error={error} />
        </div>
      )}
      {movieDetailsData ? (
        <>
          <MovieInfo
            movieInfo={movieDetailsData}
            handleModalSwitch={handleModalSwitch}
          />
          <div className="px-6 md:pl-10 md:pr-2">
            <Participants
              title={t("movieDetails.cast")}
              participants={movieDetailsData.credits.cast}
            />
            <Participants
              title={t("movieDetails.crew")}
              participants={movieDetailsData.credits.crew}
            />
            <h3 className="my-8 text-xl md:text-2xl font-bold text-center md:text-start">
              {t("movieDetails.similar")}
            </h3>
            <div className="flex gap-4 md:gap-10 mb-10 overflow-x-clip">
              <Swiper
                spaceBetween={26}
                slidesPerView="auto"
                className="h-[15em] md:h-[17em] ml-0"
              >
                {movieDetailsData.similar.results.slice(0, 9).map((item) => {
                  return (
                    <SwiperSlide
                      key={item.id}
                      className="flex justify-center items-center w-[11em]"
                    >
                      <Link
                        to={`/movie/${item.id}`}
                        onClick={() => setMovieDetailsData(null)}
                      >
                        <MovieCover
                          size="md"
                          movieTitle={item.title}
                          imageUrl={item.poster_path}
                          withBorder
                        />
                      </Link>
                    </SwiperSlide>
                  );
                })}
              </Swiper>
            </div>
          </div>
          <Modal isActive={isActiveModal} handleModalSwitch={handleModalSwitch}>
            {movieDetailsData.videos.results.length !== 0 && (
              <Video
                modalActive={isActiveModal}
                title={movieDetailsData.videos.results[0].name}
                videoSrc={`${youtubeUrl}/${movieDetailsData.videos.results[0].key}?mute=0`}
              />
            )}
          </Modal>
        </>
      ) : (
        error.length === 0 && (
          <div className="h-[80vh] flex bg-light-dark justify-center items-center">
            <Spinner />
          </div>
        )
      )}
    </>
  );
};

export default MovieDetails;
