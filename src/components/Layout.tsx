import React, { ReactNode } from "react";

type LayoutProps = {
  header: ReactNode;
  children: ReactNode;
  footer: ReactNode;
  mobileNav: ReactNode;
};

const Layout: React.FC<LayoutProps> = ({
  header,
  mobileNav,
  footer,
  children,
}) => {
  return (
    <>
      <header>{header}</header>
      {mobileNav}
      <main>{children}</main>
      <footer>{footer}</footer>
    </>
  );
};

export default Layout;
