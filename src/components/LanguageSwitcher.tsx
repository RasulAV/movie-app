import React from "react";
import config from "config";
import i18n from "i18n/i18n";
import { useDispatch } from "react-redux";
import { changeLang } from "store/languageReducer";
import { Language } from "./types";

const langParam = {
  en: "en-ES",
  ru: "ru-RU",
};

const LanguageSwitcher: React.FC = () => {
  const languages = config.language.available;
  const curLanguage = i18n.language;
  const dispatch = useDispatch();

  const handleSwitchLang = (val: keyof typeof langParam) => {
    i18n.changeLanguage(val);
    dispatch(changeLang(langParam[val] as Language));
  };

  return (
    <>
      <select
        className="p-1 rounded-lg bg-transparent border-[0.1em] border-inherit cursor-pointer"
        defaultValue={curLanguage}
        onChange={(e) =>
          handleSwitchLang(e.target.value as keyof typeof langParam)
        }
      >
        {languages.map((language) => {
          if (language.lang !== curLanguage) {
            return (
              <option key={language.lang} className="bg-light-dark">
                {language.lang}
              </option>
            );
          } else {
            return (
              <option className="hidden" key={language.lang}>
                {language.lang}
              </option>
            );
          }
        })}
      </select>
    </>
  );
};

export default LanguageSwitcher;
