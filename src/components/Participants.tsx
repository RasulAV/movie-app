import { faUser } from "@fortawesome/free-solid-svg-icons";
import config from "config";
import { ParticipantsType } from "components/types";
import { Swiper, SwiperSlide } from "swiper/react";
import Icon from "./common/Icon";

const baseImageUrl = config.service.BASE_IMAGE_URL;

type ParticipantsProps = {
  title: string;
  participants: ParticipantsType[];
};

const Participants: React.FC<ParticipantsProps> = ({ title, participants }) => {
  return (
    <>
      <h3 className="my-8 text-xl md:text-2xl font-bold text-center md:text-start">
        {title}
      </h3>
      <div className="flex gap-3 md:gap-10 overflow-x-clip">
        <Swiper
          spaceBetween={26}
          slidesPerView="auto"
          className="h-[17em] ml-0"
        >
          {participants.slice(0, 9).map((item: ParticipantsType, index) => {
            const role = title === "Crew" ? item.character : item.job;
            return (
              <SwiperSlide key={index} className="flex justify-center w-[8em]">
                <div className="flex flex-col items-center w-[7em] shrink-0">
                  <div className="w-[6.625em] h-[9em] md:h-[10em] lg:h-[11em] flex justify-center items-center">
                    {item.profile_path === null ? (
                      <Icon icon={faUser} />
                    ) : (
                      <img
                        className="w-full h-full object-cover rounded-[100vh]"
                        src={`${baseImageUrl}/w185${item.profile_path}`}
                        alt="profile"
                      />
                    )}
                  </div>
                  <p className="mt-2 text-center font-bold md:text-lg">
                    {item.name}
                  </p>
                  <p className="text-center md:text-lg">{role}</p>
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </>
  );
};

export default Participants;
