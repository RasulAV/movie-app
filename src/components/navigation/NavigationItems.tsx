import { useTranslation } from "react-i18next";

type NavigationItemsProps = {
  onMobileTogglePressed?: () => void;
};

const NavigationItems: React.FC<NavigationItemsProps> = ({
  onMobileTogglePressed,
}) => {
  const { t } = useTranslation();

  const menuItems = [
    { id: 1, title: t("home.popular"), pageLink: "popular" },
    { id: 2, title: t("home.rated"), pageLink: "trending" },
    { id: 3, title: t("home.comedy"), pageLink: "comedy" },
    { id: 4, title: t("home.action"), pageLink: "action" },
    { id: 5, title: t("home.romantic"), pageLink: "romantic" },
    { id: 6, title: t("home.crime"), pageLink: "crime" },
    { id: 7, title: t("home.horror"), pageLink: "horror" },
  ];

  return (
    <ul className="h-full flex flex-col md:flex-row justify-around lg:gap-2 md:gap-4">
      {menuItems.map((item) => {
        return (
          <li
            key={item.id}
            className="text-xl md:text-base lg:text-lg text-secondary md:text-primary font-medium cursor-pointer hover:text-danger"
          >
            <a href={`/#${item.pageLink}`} onClick={onMobileTogglePressed}>
              {item.title}
            </a>
          </li>
        );
      })}
    </ul>
  );
};

export default NavigationItems;
