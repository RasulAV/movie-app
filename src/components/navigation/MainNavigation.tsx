import { useEffect, useState } from "react";
import { faFilm } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from "react-i18next";
import classNames from "classnames";
import { Link, useNavigate } from "react-router-dom";
import { routePaths } from "routes/routePaths";
import Icon from "components/common/Icon";
import NavigationItems from "./NavigationItems";
import MobileToggle from "./MobileToggle";
import Input from "components/common/Input";
import LanguageSwitcher from "components/LanguageSwitcher";

type MainNavigationProps = {
  isOpenMobileNav: boolean;
  onMobileTogglePressed: () => void;
};

const MainNavigation: React.FC<MainNavigationProps> = ({
  isOpenMobileNav,
  onMobileTogglePressed,
}) => {
  const [searchValue, setSearchValue] = useState("");
  const [prevPage, setPrevPage] = useState("");
  const [isScrolled, setIsScrolled] = useState(false);
  const { t } = useTranslation();
  const navigate = useNavigate();

  window.onscroll = () => {
    if (window.scrollY > 50) {
      setIsScrolled(true);
    } else {
      setIsScrolled(false);
    }
  };

  useEffect(() => {
    const savePrevPage = (nextPage: string) => {
      if (nextPage !== routePaths.searchingResult) {
        setPrevPage(nextPage);
      }
    };

    if (searchValue.trim().length !== 0) {
      savePrevPage(window.location.pathname);
      navigate(routePaths.searchingResult, {
        state: { searchingValue: searchValue },
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchValue]);

  const navigateToPrevPage = (newSearchValue: string) => {
    if (
      newSearchValue.trim().length === 0 &&
      window.location.pathname === routePaths.searchingResult &&
      prevPage.length !== 0
    ) {
      navigate(prevPage);
    }
  };

  const changeSearchValue = (newSearchValue: string) => {
    setSearchValue(newSearchValue);
    navigateToPrevPage(newSearchValue);
  };

  const clearSearchValue = () => {
    setSearchValue("");
  };

  return (
    <nav
      className={classNames(
        "fixed w-full flex items-center z-[2] p-3 md:p-4 transition duration-500 ease-in-out",
        isOpenMobileNav ? "justify-end" : "justify-between",
        isScrolled && "bg-light-dark"
      )}
    >
      {!isOpenMobileNav && (
        <>
          <Link
            className="flex justify-center"
            to={`/`}
            onClick={clearSearchValue}
          >
            <Icon icon={faFilm} size="md" />
          </Link>
          <div className="hidden md:block">
            <NavigationItems />
          </div>
          <Input
            searchValue={searchValue}
            changeSearchValue={changeSearchValue}
            placeholder={t("navigation.search")}
          />
          <LanguageSwitcher />
        </>
      )}
      <div className="md:hidden">
        <MobileToggle
          isOpenMobileNav={isOpenMobileNav}
          onMobileTogglePressed={onMobileTogglePressed}
        />
      </div>
    </nav>
  );
};

export default MainNavigation;
