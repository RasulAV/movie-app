import classNames from "classnames";

type MobileToggleProps = {
  isOpenMobileNav: boolean;
  onMobileTogglePressed: () => void;
};

const MobileToggle: React.FC<MobileToggleProps> = ({
  isOpenMobileNav: isOpen,
  onMobileTogglePressed,
}) => {
  const commonStyles =
    "w-8 h-0.5 bg-grey-100 rounded-md transform transition ease-in-out origin-[0%_0%]";

  return (
    <div
      className={classNames(
        "space-y-[0.55em]",
        isOpen && "h-screen pt-[0.6em] pl-8"
      )}
      onTouchStart={onMobileTogglePressed}
    >
      <div
        className={classNames(
          "duration-500",
          commonStyles,
          isOpen && "rotate-45"
        )}
      ></div>
      <div
        className={classNames(
          commonStyles,
          "duration-200",
          isOpen && "scale-y-0"
        )}
      ></div>
      <div
        className={classNames(
          "duration-500",
          commonStyles,
          isOpen && "-rotate-45 origin-[0%_100%]"
        )}
      ></div>
    </div>
  );
};

export default MobileToggle;
