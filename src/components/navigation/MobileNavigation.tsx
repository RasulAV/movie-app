import classNames from "classnames";
import NavigationItems from "./NavigationItems";

type MobileNavigationProps = {
  isOpenMobileNav: boolean;
  onMobileTogglePressed: () => void;
};

const MobileNavigation: React.FC<MobileNavigationProps> = ({
  isOpenMobileNav,
  onMobileTogglePressed,
}) => {
  return (
    <nav
      className={classNames(
        "fixed h-screen w-5/6 top-0 z-10 text-center space-y-5 pt-5 px-5 bg-grey-100 transform duration-500",
        isOpenMobileNav ? "left-0" : "-left-full"
      )}
    >
      <NavigationItems onMobileTogglePressed={onMobileTogglePressed} />
    </nav>
  );
};

export default MobileNavigation;
