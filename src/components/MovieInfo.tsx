import config from "config";
import { GenresType, MovieInfoType, ParticipantsType } from "components/types";
import { useTranslation } from "react-i18next";
import Button from "./common/Button";
import MovieCover from "./common/MovieCover";

const imageUrl = config.service.BASE_IMAGE_URL;

type MovieInfoProps = {
  movieInfo: MovieInfoType;
  handleModalSwitch: (value: boolean) => void;
};

const MovieInfo: React.FC<MovieInfoProps> = ({
  movieInfo,
  handleModalSwitch,
}) => {
  const { t } = useTranslation();

  const {
    backdrop_path,
    original_title: title,
    poster_path,
    credits,
    runtime,
    genres,
    overview,
    videos,
  } = movieInfo;

  const parseGenres = (genres: GenresType[]) => {
    return genres.map((genre) => genre.name).join(" | ");
  };

  const parseRuntime = (totalMinute: number) => {
    const hours = Math.floor(totalMinute / 60);
    const strHours = hours === 0 ? "" : `${hours}h`;
    const strMinutes = `${totalMinute % 60}m`;
    return `${strHours} ${strMinutes}`;
  };

  const findDirectors = (crew: ParticipantsType[]) => {
    const directors = crew.reduce((directors, person) => {
      if (person.job === "Director") {
        directors.push(person.name);
      }
      return directors;
    }, [] as string[]);
    return directors.join(", ");
  };

  const handleCreateDirectorItem = (directors: string) => {
    if (directors.length !== 0) {
      return (
        <h3 className="text-lg md:text-xl xl:text-2xl">{`${t(
          "movieDetails.directed"
        )} ${directors}`}</h3>
      );
    }
  };

  const backdropStyle =
    backdrop_path === null
      ? { backgroundColor: "transparent" }
      : {
          backgroundImage: `linear-gradient(0deg, rgb(20, 20, 20) 4%, rgba(20, 20, 20, 0.46) 100%), url(${imageUrl}/w1280/${backdrop_path})`,
        };

  return (
    <div
      className="flex flex-col md:flex-row items-center gap-10 pb-2 pt-24 px-6 md:pl-10 md:pr-16 bg-cover bg-top"
      style={{ ...backdropStyle }}
    >
      <MovieCover
        clickable={false}
        movieTitle="name"
        size="lg"
        imageUrl={poster_path}
      />
      <div className="flex flex-col gap-4 items-center lg:items-start">
        <h1 className="text-center lg:text-start text-3xl md:text-4xl xl:text-5xl font-bold">
          {title}
        </h1>
        {handleCreateDirectorItem(findDirectors(credits.crew))}
        <h4 className="text-base md:text-xl">{parseRuntime(runtime)}</h4>
        <h4 className="text-base md:text-xl">{parseGenres(genres)}</h4>
        {videos.results.length !== 0 && (
          <Button
            title={t("movieDetails.playTrailer")}
            onClick={() => handleModalSwitch(true)}
          />
        )}
        <h4 className="text-justify text-base md:text-xl pt-3">{overview}</h4>
      </div>
    </div>
  );
};

export default MovieInfo;
