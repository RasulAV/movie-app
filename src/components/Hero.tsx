import { Link } from "react-router-dom";
import Button from "./common/Button";
import config from "config";
import Spinner from "./common/Spinner";
import { useTranslation } from "react-i18next";
import { MovieListItemType } from "./types";

type HeroProps = {
  heroData: MovieListItemType;
};

const Hero: React.FC<HeroProps> = ({ heroData }) => {
  const { id, title, overview, backdrop_path } = heroData;
  const { t } = useTranslation();

  const backdropStyle =
    heroData?.backdrop_path === undefined || heroData?.backdrop_path === null
      ? { backgroundColor: "transparent" }
      : {
          backgroundImage: `linear-gradient(0deg, rgb(20, 20, 20) 4%, rgba(20, 20, 20, 0.46) 100%), url(${config.service.BASE_IMAGE_URL}/w1280/${backdrop_path})`,
        };

  return (
    <>
      {Object.keys(heroData).length ? (
        <div
          className="w-full h-full sm:h-[80vh] pt-[8em] md:pt-[10em] px-[2em] md:px-[3em] lg:pl-[5em] lg:pr-[3em] bg-cover bg-top object-contain"
          style={{ ...backdropStyle }}
        >
          <h1 className="pb-[0.4em] text-primary text-4xl md:text-5xl lg:text-7xl font-bold leading-[1.2em]">
            {title}
          </h1>
          <Link to={`movie/${id}`}>
            <Button title={t("hero.details")} size="md" />
          </Link>
          {overview.length !== 0 && (
            <div className="w-full md:w-2/3 lg:w-3/5 pt-[1em]">
              <p className="text-primary md:text-lg font-medium">{`${overview.substring(
                0,
                149
              )}...`}</p>
            </div>
          )}
        </div>
      ) : (
        <div className="h-[80vh] flex bg-light-dark text-primary justify-center items-center">
          <Spinner />
        </div>
      )}
    </>
  );
};

export default Hero;
