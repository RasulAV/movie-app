import axiosInstance from "api/axiosInstance";
import errorHandler from "api/errorHandler";
import { AxiosError } from "axios";
import { MovieListItemType } from "components/types";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import Error from "./common/Error";
import MovieCover from "./common/MovieCover";
import Spinner from "./common/Spinner";
import useDebounce from "./hooks/useDebounce";

type SearchingParams = {
  searchingValue: string;
};

const SearchingResult: React.FC = () => {
  const [searchingMovies, setSearchingMovies] = useState<MovieListItemType[]>(
    []
  );
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const location = useLocation();
  const { t } = useTranslation();

  const searchingParams =
    location.state !== null ? (location.state as SearchingParams) : undefined;

  const debouncedValue = useDebounce<string | undefined>(
    searchingParams?.searchingValue,
    1000
  );

  const fetchData = async (searchValue: string) => {
    const parameters = {
      include_adult: false,
      query: searchValue,
    };
    await axiosInstance("/search/movie", { params: parameters })
      .then((response) => {
        setSearchingMovies(response.data.results.slice(0, 9));
      })
      .catch((err) => {
        const newError = errorHandler(err as AxiosError | Error);
        setError(newError);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    if (debouncedValue !== undefined) {
      setIsLoading(true);
      fetchData(debouncedValue);
    }
  }, [debouncedValue]);

  return (
    <div className="min-h-[83vh]">
      {error.length !== 0 ? (
        <div className="flex justify-center items-center min-h-[83vh]">
          <Error error={error} />
        </div>
      ) : isLoading ? (
        <div className="h-[80vh] flex justify-center items-center">
          <Spinner />
        </div>
      ) : !searchingMovies.length ? (
        <div className="flex justify-center items-center min-h-[83vh]">
          <p className="text-xl text-center">{t("searching.unknownResult")}</p>
        </div>
      ) : (
        <div className="flex flex-col items-center pt-24">
          <h1 className="mb-7 text-center text-3xl font-bold">
            {t("searching.movieList")}
          </h1>
          <div className="flex flex-wrap items-center justify-center gap-8 ">
            {searchingMovies.map((movie) => {
              return (
                <Link to={`/movie/${movie.id}`} key={movie.id}>
                  <MovieCover
                    key={movie.id}
                    size="md"
                    movieTitle={movie.original_title}
                    imageUrl={movie.poster_path}
                  />
                </Link>
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
};

export default SearchingResult;
