import React, { useEffect, useState } from "react";
import { AxiosError } from "axios";
import Error from "./common/Error";
import axiosInstance from "api/axiosInstance";
import errorHandler from "api/errorHandler";
import Hero from "./Hero";
import MoviesList from "./MoviesList";
import { MovieListItemType, HomeTitles } from "./types";
import { useSelector } from "react-redux";
import { languageSelector } from "store/selectors";

type PageLink = string;
type MovieListData = { [key: string]: [MovieListItemType[], PageLink?] };
type Requests = {
  path: string;
  title: HomeTitles;
  pageLink?: string;
  params?: { [key: string]: string };
};

const Home: React.FC = () => {
  const [movieListData, setMovieListData] = useState<MovieListData>({});
  const [error, setError] = useState<string>("");
  const [heroData, setHeroData] = useState<MovieListItemType>(
    {} as MovieListItemType
  );
  const curLang = useSelector(languageSelector);

  const parseDate = (value: number) => {
    return value.toString().length === 1 ? `0${value}` : value;
  };
  const today = new Date();
  const parsedDate = `${today.getFullYear()}-${parseDate(
    today.getMonth() + 1
  )}-${parseDate(today.getDay())}`;

  const catchError = (error: string) => {
    setError(error);
  };

  const requests: Requests[] = [
    { path: "movie/popular", title: "popular", pageLink: "popular" },
    {
      path: "/trending/movie/week",
      title: "trending",
      pageLink: "trending",
    },
    {
      path: "discover/movie",
      title: "comedy",
      params: { with_genres: "35" },
      pageLink: "comedy",
    },
    {
      path: "discover/movie",
      title: "action",
      params: { with_genres: "28" },
      pageLink: "action",
    },
    {
      path: "discover/movie",
      title: "romantic",
      params: { with_genres: "10749" },
      pageLink: "romantic",
    },
    {
      path: "discover/movie",
      title: "crime",
      params: { with_genres: "80" },
      pageLink: "crime",
    },
    {
      path: "discover/movie",
      title: "horror",
      params: { with_genres: "27" },
      pageLink: "horror",
    },
    {
      path: "discover/movie",
      title: "documentaries",
      params: { with_genres: "99" },
    },
    {
      path: "/discover/movie",
      title: "new",
      params: { sort_by: "release_date.desc", "release_date.lte": parsedDate },
    },
  ];

  useEffect(() => {
    const fetchMoviesListData = async (
      url: string,
      title: HomeTitles,
      params?: { [key: string]: string | undefined },
      pageLink?: string
    ) => {
      await axiosInstance(url, { params: params })
        .then((response) => {
          let newVal: MovieListData = {};
          newVal[title] = [response.data.results.slice(0, 10), pageLink];
          setMovieListData((prev: MovieListData) => ({ ...prev, ...newVal }));
        })
        .catch((err) => {
          const newError = errorHandler(err as AxiosError | Error);
          catchError(newError);
        });
    };

    requests.forEach((request) => {
      fetchMoviesListData(
        request.path,
        request.title,
        request.params,
        request.pageLink
      );
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const fetchHeroData = async () => {
      await axiosInstance(`/movie/top_rated?page=1`, {
        params: { language: curLang },
      })
        .then((response) => {
          if (response.data.results.length !== 0) {
            setHeroData(
              response.data.results[
                Math.floor(Math.random() * response.data.results.length)
              ]
            );
          } else {
            catchError("404");
          }
        })
        .catch((err) => {
          const newError = errorHandler(err as AxiosError | Error);
          catchError(newError);
        });
    };
    fetchHeroData();
  }, [curLang]);

  return (
    <>
      {error.length ? (
        <div className="pt-24">
          <Error error={error} />
        </div>
      ) : (
        <>
          <Hero heroData={heroData} />
          {Object.keys(movieListData).map((movieKey: string) => {
            return (
              <MoviesList
                key={movieKey}
                title={movieKey as HomeTitles}
                moviesList={movieListData[movieKey]}
              />
            );
          })}
        </>
      )}
    </>
  );
};

export default Home;
