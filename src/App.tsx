import { useState } from "react";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes/routes";
import { store } from "./store/store";
import ErrorBoundary from "./components/ErrorBoundary";
import Layout from "./components/Layout";
import MainNavigation from "./components/navigation/MainNavigation";
import MobileNavigation from "./components/navigation/MobileNavigation";
import Footer from "./components/Footer";
import ScrollToTop from "components/hooks/ScrollToTop";

function App() {
  const [isOpenMobileNav, setIsOpenMobileNav] = useState(false);

  const onMobileTogglePressed = () => {
    setIsOpenMobileNav(!isOpenMobileNav);
  };

  return (
    <Provider store={store}>
      <BrowserRouter>
        <ErrorBoundary>
          <ScrollToTop />
          <Layout
            header={
              <MainNavigation
                isOpenMobileNav={isOpenMobileNav}
                onMobileTogglePressed={onMobileTogglePressed}
              />
            }
            mobileNav={
              <MobileNavigation
                isOpenMobileNav={isOpenMobileNav}
                onMobileTogglePressed={onMobileTogglePressed}
              />
            }
            footer={<Footer />}
          >
            <Routes />
            <ToastContainer autoClose={3000} />
          </Layout>
        </ErrorBoundary>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
