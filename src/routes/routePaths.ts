export const routePaths = {
  movies: "/",
  movieDetails: "/movie/:id",
  searchingResult: "/search",
};
