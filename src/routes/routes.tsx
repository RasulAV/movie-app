import Home from "components/Home";
import MovieDetails from "components/MovieDetails";
import SearchingResult from "components/SearchingResult";
import { ReactNode } from "react";
import { Routes, Route } from "react-router-dom";
import { routePaths } from "./routePaths";

const allRoutes: RouteItem[] = [
  {
    id: "MOVIES",
    path: routePaths.movies,
    component: <Home />,
  },
  {
    id: "MOVIEDETAILS",
    path: routePaths.movieDetails,
    component: <MovieDetails />,
  },
  {
    id: "SEARCHINGRESULT",
    path: routePaths.searchingResult,
    component: <SearchingResult />,
  },
];

type RouteItem = {
  id: string;
  path: string;
  component: ReactNode;
};

const routes = () => {
  const unknownPath = <h3 className="text-center">Page not found</h3>;

  return (
    <Routes>
      {allRoutes.map((routeItem) => {
        return (
          <Route
            key={routeItem.id}
            path={routeItem.path}
            element={routeItem.component}
          />
        );
      })}
      <Route path="*" element={unknownPath} />
    </Routes>
  );
};

export default routes;
