import { rest } from "msw";

const fetchHero = rest.get(`/movie/top_rated`, async (req, res, ctx) => {
  return res(
    ctx.status(200),
    ctx.json({
      page: 2,
      results: [
        {
          adult: false,
          backdrop_path: "/hpzQHv8cA7j2Dn2CphOFYmllXzj.jpg",
          genre_ids: [18],
          id: 207,
          original_language: "en",
          original_title: "Dead Poets Society",
          overview:
            "At an elite, old-fashioned boarding school in New England, a passionate English teacher inspires his students to rebel against convention and seize the potential of every day, courting the disdain of the stern headmaster.",
          popularity: 36.12,
          poster_path: "/ai40gM7SUaGA6fthvsd87o8IQq4.jpg",
          release_date: "1989-06-02",
          title: "Dead Poets Society",
          video: false,
          vote_average: 8.3,
          vote_count: 9367,
        },
      ],
      total_pages: 515,
      total_results: 10294,
    })
  );
});

export const handlers = [fetchHero];
