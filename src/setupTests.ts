import { mswServer } from "./mocks/server";
import "./i18n/i18n";

beforeAll(() => {
  mswServer.listen({
    onUnhandledRequest: "warn",
  });
});
afterEach(() => mswServer.resetHandlers());
afterAll(() => mswServer.close());
